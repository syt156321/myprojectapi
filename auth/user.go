package auth

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func Login(c *gin.Context)  {
	type requestDate struct {
		Password string `form:"password" json:"password"`
		Account string `form:"account" json:"account"`
	}
	var data requestDate

	if err := c.ShouldBind(&data); err == nil{
		c.JSON(http.StatusOK, gin.H{"token": "48b88674043db68779bfb3db6877b3db68773db6877ccdb68779bfb3db6877bcc542519db68779bf3db6877bb3db6877cce"})
	}else{
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
}

func Register(c *gin.Context)  {
	c.JSON(http.StatusOK, gin.H{"status": "you are logged in"})
}

func GerInfo(c *gin.Context)  {
	type requestDate struct {
		Password string
		Account string
	}
	var data requestDate

	if err := c.ShouldBind(&data); err == nil{
		c.JSON(http.StatusOK, gin.H{"message": "Hello Golang! Hello Gin"})
	}else{
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
}