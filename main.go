package main

import "github.com/gin-gonic/gin"
import (
	"./middleWare"
	"./auth"
)

func main()  {

	gin.SetMode(gin.DebugMode)
	router := gin.Default()
	authorized  := router.Group("/api")

	authorized.Use(middleWare.Auth())
	{
		authorized.GET("/getInfo", auth.GerInfo)
		authorized.POST("/login", auth.Login)
	}

	// Listen and serve on 0.0.0.0:8080
	router.Run(":8081")
}